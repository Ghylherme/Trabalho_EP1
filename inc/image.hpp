#ifndef IMAGE_HPP
#define IMAGE_HPP

class Image {

//Atributos
private:
	int largura;
	int altura;
	int quantidade_cores; 

//Métodos
public:

	Image();
	Image(int largura, int altura, int quantidade_cores);
	~Image();
	void setLargura(int largura);
	int getLargura();
	void setAltura(int altura);
	int getAltura();
	void setQuantidade_cores(int quantidade_cores);
	int getQuantidade_cores();
	void Imprimir();
};

#endif

