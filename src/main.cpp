#include "image.hpp"
#include <iostream>

using namespace std;

int main(int argc, char ** argv){

	Image imagem1(15, 20, 255);
	Image * imagem2 = new Image(200, 155, 255);

	imagem1.Imprimir();
	imagem2->Imprimir();

	delete imagem2;

return 0;
}
