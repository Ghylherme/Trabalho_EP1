#include "image.hpp"
#include <string>
#include <iostream>

using namespace std;

Image::Image(){
//Construtor
	largura = 0;
	altura = 0;
	quantidade_cores = 0;

}
Image::Image(int largura, int altura, int quantidade_cores){
	this->largura = largura;
	this->altura = altura;
	this->quantidade_cores = quantidade_cores;
}
Image::~Image(){
	cout << "DESTRUTOR DA CLASSE IMAGE" << "\n";
}
void Image::setLargura(int largura){
	this->largura = largura;
}
int Image::getLargura(){
	return largura;
}
void Image::setAltura(int altura){
	this->altura = altura;
}
int Image::getAltura(){
	return altura;
}
void Image::setQuantidade_cores(int quantidade_cores){
	this->quantidade_cores = quantidade_cores;
}
int Image::getQuantidade_cores(){
	return quantidade_cores;
}
void Image::Imprimir(){

	cout << "Largura: " << largura << "\n";
	cout << "Altura: " << altura << "\n";
	cout << "Quantidade Máxima de cores: " << quantidade_cores << "\n";
}
